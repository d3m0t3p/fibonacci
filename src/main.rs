use std::io;
use std::vec;

fn main() {
    println!("Hello, world!");
    println!(
        "This is the fibonnaci sequence
Please input the number of therme you'd like"
    );

    loop {
        let mut n = String::new();
        io::stdin().read_line(&mut n).expect("Failed to read line");

        let n: usize = match n.trim().parse() {
            Ok(nu) => nu,
            Err(_) => {
                println!("Unable to parse your input, please type a positive number");
                continue;
            }
        };

        println!("{}th termes is being generated... \n\n", n);
        fibonacci_three_way(n);
        print!("{}", fibonnaci_rec(n));
        break;
    }
}

fn fibonnaci_rec(nth: usize) -> usize {
    if nth == 1 {
        return 1;
    } else if nth == 2 {
        return 1;
    } else {
        return fibonnaci_rec(nth - 1) + fibonnaci_rec(nth - 2);
    }
}

fn fibonacci_three_way(nth: usize) {
    let mut i: u128 = 0;
    let mut i1: u128 = 1;
    let mut i2: u128 = 1;

    let nth = nth - 2;

    let l = nth / 3;
    let r = nth % 3;

    for _ in 0..l {
        i = i1 + i2;
        i1 = i + i2;
        i2 = i1 + i;
    }

    if r == 2 {
        i = i1 + i2;
        i1 = i + i2;
        println!("{}", i1);
    } else if r == 1 {
        i = i1 + i2;
        println!("{}", i);
    } else {
        println!("{}", i2);
    }
}

fn generate_fibonacci_array(nth: usize) {
    let mut fib: Vec<u128> = vec![0; nth];
    fib[0] = 1;
    fib[1] = 1;

    let mut i = 2;

    while i <= nth {
        fib[i] = fib[i - 1] + fib[i - 2];
        i = i + 1;
    }
    println!("{}", fib[nth - 1]);
}
